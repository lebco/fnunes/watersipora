# Watersipora phylogeny based on COI

## Description
These are the analysis steps used in the article:

First joint morphological and molecular detection of _Watersipora subatra_ in the Mediterranean Sea presented in an updated genus phylogeny to resolve taxonomic confusion.

Aquatic Invasions


## Sequence Alignment
sequence aligment was conducted using MAFFT v7.490

```
INFILE=watersipora-coi-99-unique.fas
OUTFILE=watersipora-coi-99-unique-align.fas

mafft --localpair --maxiterate 1000 $INFILE >& $OUTFILE
```


## Maximum Likelihood Phylogenetic Analysis

ML phylogenetic analysis was conducted using iqtree v2.0.3

```
iqtree -s watersipora-coi-99-unique-align.fas -m HKY+G4 -B 1000
``` 
The output tree is:

```
watersipora-coi-99-unique-align.fas.contree
```

## Baysian Phylogenetic Analysis

Bayesian phylogenetic analysis was conducted using BEAST v.1.10.4

The xml file containing all parameters used for the run is:

```
watersipora-coi-99-unique-beastv1104-107.xml
```
The consensus tree can be found in this file:

```
watersipora-coi-99-unique-align.fas.contree
```

## Support
Contact flavia.nunes@ifremer.fr if you have any questions about the data or analysis tools.

## Authors and acknowledgment
Robin Gauff
Marc Bouchoucha
Gabin Droual   https://orcid.org/0000-0002-5844-2579
Amelia Curd   https://orcid.org/0000-0003-3260-7192
Justine Evrard
Nicolas Gayet
Flavia Nunes   https://orcid.org/0000-0002-3947-6634

## License
please cite our paper if you use any of the data provided here.
CC-BY-NC
